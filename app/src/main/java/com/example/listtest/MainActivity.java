package com.example.listtest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    String[] mobileArray = {"Android", "IPhone", "WindowsMobile", "Blackberry", "WebOS", "Ubuntu", "Windows7", "MaC OS X"};
    String[] mobileDescriptionDetails = {"Body: Aluminium body with front glass and plastic ends\n" +
            "\n" +
            "SIM: Dual SIM\n" +
            "\n" +
            "Display screen:  \n" +
            "\n" +
            "Type -- LCD, touch screen\n" +
            "\n" +
            "Size – 5 inches\n" +
            "\n" +
            "Resolution: 720 x 1820 pixels\n" +
            "\n" +
            "Platform:  \n" +
            "\n" +
            "OS: Android 5.1\n" +
            "\n" +
            "Chipset: Qualcomm/Snapdragon\n" +
            "\n" +
            "CPU: Octa-core  \n" +
            "\n" +
            "Memory:  \n" +
            "\n" +
            "Internal: 16 GB with 2 GB RAM\n" +
            "\n" +
            "Extended: up to 32 GB\n" +
            "\n" +
            "Camera:  13 megapixels with HDR, panoramic features\n"
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayAdapter adapter = new ArrayAdapter<>(this,
                R.layout.activity_listview, mobileArray);

        ListView listView = findViewById(R.id.mobile_list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String mobiles = String.valueOf(parent.getItemAtPosition(position));
                matchTheValue(mobiles);
            }
        });
    }

    private void matchTheValue(String position) {
        Intent goToNextActivity = new Intent(MainActivity.this, DetailActivity.class);
        switch (position) {
            case "Android":
            case "MacOs":
            case "iphone":
            case "WindowsMobile":
            case "Blackberry":
            case "WebOS":
            case "Ubuntu":
            case "Windows7":
                nexActivity(goToNextActivity, mobileDescriptionDetails[0]);
                break;
        }
    }

    private void nexActivity(Intent goToNextActivity, String description) {
        goToNextActivity.putExtra("mobileDescription", description);
        startActivity(goToNextActivity);
    }


}
